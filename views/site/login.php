<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;


$form = ActiveForm::begin([
    'id' => 'login-form',
    'options' => [
        'class' => 'form-auth-small'
    ],
]); ?>
<div class="form-group">
    <label for="signin-email" class="control-label sr-only">Email</label>
    <?= $form->field($model, 'username')->textInput(['class' => 'form-control', 'required' => true, 'style' => 'background-color: #fff !important;', 'placeholder' => "profile Username"]) ?>
</div>

<?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-lg btn-block', 'name' => 'login-button']) ?>


<?php ActiveForm::end(); ?>
<br>
<div class="col-lg-offset-1" style="color:#999;">
        You may login with <strong>mukhwanak</strong>  or <strong>manolo</strong> for demo or any if you have</strong>.<br>

    </div>
