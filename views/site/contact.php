<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Opportunity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row clearfix">
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="body">
                <h5><?= $opportunity["objective"] ?></h5>
                <p></p>

            </div>
        </div>
        <div class="card">
            <div class="body">
                <ul class=" list-unstyled basic-list">
                    <li>Compensation (<?= $opportunity["compensation"]["periodicity"] ?>):<span
                                class="badge badge-primary"><?= $opportunity["compensation"]["currency"] . ' ' . $opportunity["compensation"]["minAmount"] . '-' . $opportunity["compensation"]["maxAmount"] ?></span>
                    </li>
<!--                    <li>Created:<span class="badge-purple badge">14 Mar, 2018</span></li>-->
                    <li>Deadline:<span class="badge-purple badge"><?= \Yii::$app->formatter->asDate($opportunity["deadline"],'long')?></span></li>
<!--                    <li>Priority:<span class="badge-danger badge">Highest priority</span></li>-->
<!--                    <li>Status<span class="badge-info badge">Working</span></li>-->
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>Members</h2>
            </div>
            <div class="body">
                <div class="w_user">
                    <img class="rounded-circle" src="<?= Url::to($opportunity["owner"]["picture"]) ?>" alt="">
                    <div class="wid-u-info">
                        <h5><?= $opportunity["owner"]["name"] ?></h5>
                        <span>Username: <?= $opportunity["owner"]["username"] ?></span>
                        <p class="text-muted m-b-0"><?= $opportunity["owner"]["professionalHeadline"] ?></p>
                    </div>
                    <br>
                    <hr>
                </div>
                <ul class="right_chat list-unstyled mb-0">
                    <?php
                    if (!empty($opportunity["members"])) {
                        foreach ($opportunity["members"] as $members) {
                            ?>
                            <li class="online">
                                <a href="javascript:void(0);">
                                    <div class="media">
<!--                                        <img class="media-object " src="--><?//= Url::to($members["person"]["picture"]) ?><!--" alt="">-->
                                        <div class="media-body">
                                            <span class="name"><?= $members["person"]["name"]?></span>
<!--                                            <span class="message">--><?//= $members["person"]["professionalHeadline"]?><!--</span>-->
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td>Non Members</td>
                        </tr>
                        <?php
                    }
                    ?>

                </ul>
            </div>
        </div>
        <div class="card">
            <div class="header">
                <h2>Organization</h2>
            </div>
            <div class="body text-center">
                <div class="profile-image m-b-15"><img src="<?= Url::to($opportunity["organizations"][0]["picture"]) ?>"
                                                       class="rounded-circle" alt="">
                </div>
                <div>
                    <h4 class="m-b-0"><strong><?= $opportunity["organizations"][0]["name"] ?></strong></h4>

                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Info</h2>
            </div>
            <?php
            if (!empty($opportunity["details"])) {
                foreach ($opportunity["details"] as $details) {
                    ?>
                    <div class="body">
                        <h4><?= $details["code"] ?></h4>
                        <p> <?= $details["content"] ?></p>

                    </div>
                    <hr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td>No Education Registered</td>
                </tr>
                <?php
            }
            ?>

        </div>

    </div>
</div>

