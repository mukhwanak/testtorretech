<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Bio';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="row clearfix">
    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="header">
                <h2>languages</h2>
            </div>
            <div class="body">
                <table class="table m-b-0">
                    <tbody>
                    <?php
                    if (!empty($bio["languages"])) {
                        foreach ($bio["languages"] as $language) {
                            ?>
                            <tr>
                                <td><?= $language["language"] ?></td>
                                <td class="align-right"><?= $language["fluency"] ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td>No Language Registered</td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-12">
        <div class="card w_profile">
            <div class="body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-12">
                        <div class="profile-image float-md-right"><img src="<?= Url::to($bio["person"]["picture"]) ?>"
                                                                       alt=""></div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-12">
                        <h4 class="m-t-0 m-b-0"><strong><?= $bio["person"]["name"] ?></strong></h4>
                        <span class="job_post"><?= $bio["person"]["professionalHeadline"] ?></span>
                        <p><?= $bio["person"]["location"]["name"] ?>,
                            Timezone: <?= $bio["person"]["location"]["timezone"] ?></p>
                        <!--                        <p class="social-icon">-->
                        <!--                            <a title="Twitter" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>-->
                        <!--                            <a title="Facebook" href="javascript:void(0);"><i class="fa fa-facebook"></i></a>-->
                        <!--                            <a title="Google-plus" href="javascript:void(0);"><i class="fa fa-twitter"></i></a>-->
                        <!--                            <a title="Behance" href="javascript:void(0);"><i class="fa fa-behance"></i></a>-->
                        <!--                            <a title="Instagram" href="javascript:void(0);"><i class="fa fa-instagram "></i></a>-->
                        <!--                        </p>-->
                        <div class="row">
                            <div class="col-3">
                                <h5><?= $bio["stats"]["jobs"] ?></h5>
                                <small>Jobs</small>
                            </div>
                            <div class="col-3">
                                <h5><?= $bio["stats"]["strengths"] ?></h5>
                                <small>Strengths</small>
                            </div>
                            <div class="col-3">
                                <h5><?= $bio["stats"]["interests"] ?></h5>
                                <small>Interests</small>
                            </div>

                            <div class="col-3">
                                <h5><?= $bio["stats"]["education"] ?></h5>
                                <small>Education</small>
                            </div>
                        </div>
                        <!--                        <div class="m-t-15">-->
                        <!--                            <button class="btn btn-primary">Follow</button>-->
                        <!--                            <button class="btn btn-success">Message</button>-->
                        <!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row clearfix">
    <div class="col-lg-3 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Strengths</h2>
            </div>
            <div class="body">
                <?php
                if (!empty($bio["strengths"])) {
                    foreach ($bio["strengths"] as $strengths) {
                        ?>
                        <!--                            <div class="alert alert-primary" role="alert">--><?//= $strengths["name"] ?><!--</div>-->
                        <td><span class="badge badge-primary"> <?= $strengths["name"] ?> </span></td>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td>Non Registered</td>
                    </tr>
                    <?php
                }
                ?>


            </div>
        </div>

    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Education</h2>
            </div>
            <?php
            if (!empty($bio["education"])) {
                foreach ($bio["education"] as $education) {
                    ?>
                    <div class="body">
                        <h4><?= $education["name"] ?></h4>
                        <small><?= $education["organizations"][0]["name"] ?></small>
                        <p> <?php
                           if(!empty($organizations["additionalInfo"])) {
                               echo $organizations["additionalInfo"];
                           }
                             ?></p>
                        <span> <?= $education["fromMonth"] ?> ,<?= $education["fromYear"] ?> </span> to
                        <span> <?= $education["toMonth"] ?> ,<?= $education["toYear"] ?> </span>
                    </div>
                    <hr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td>No Education Registered</td>
                </tr>
                <?php
            }
            ?>

        </div>

        <div class="card">
            <div class="header">
                <h2>Jobs</h2>
            </div>
            <?php
            if (!empty($bio["jobs"])) {

                foreach ($bio["jobs"] as $job) {

                    ?>
                    <div class="body">
                        <h4><?= $job["name"] ?></h4>
                        <small><?= $job["organizations"][0]["name"] ?></small>
                        <p> <?php
                           if(!empty($job["additionalInfo"])) {
                               echo $job["additionalInfo"];
                           }
                             ?></p>
                        <span> <?= $job["fromMonth"] ?> ,<?= $job["fromYear"] ?> </span> to
                        <!--                        <span> --><?//= $job["toMonth"] ?><!-- ,-->
                        <?//= $job["toYear"] ?><!-- </span>-->
                    </div>
                    <hr>
                    <?php
                }
            } else {
                ?>
                <tr>
                    <td>No Language Registered</td>
                </tr>
                <?php
            }
            ?>

        </div>
    </div>

    <div class="col-lg-3 col-md-12">
        <div class="card">
            <div class="header">
                <h2>Interests</h2>
            </div>
            <div class="body">
                <?php
                if (!empty($bio["interests"])) {
                    foreach ($bio["interests"] as $interests) {
                        ?>
                        <td><span class="badge badge-info"> <?= $interests["name"] ?> </span></td>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td>Non Interest</td>
                    </tr>
                    <?php
                }
                ?>


            </div>
        </div>
    </div>
