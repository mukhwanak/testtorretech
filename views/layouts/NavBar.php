<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 6/24/2020
 * Time: 5:42 PM
 */

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

?>
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-brand">
                Torre
                <!--                <a href="index.html"><img src="../assets/images/logo.svg" alt="Lucid Logo" class="img-responsive logo"></a>-->
            </div>
            <div class="navbar-right">
                <div id="navbar-menu">
                    <?php
                    NavBar::begin([
                        'options' => [
                            'class' => 'nav navbar-nav',
                        ],
                    ]);

                    echo Nav::widget([
//                        'options' => ['class' => 'nav navbar-nav'],
                        'items' => [
//                            ['label' => 'Home', 'url' => ['/site/index']],
                            ['label' => 'Bio', 'url' => ['/site/about']],
                            ['label' => 'Opportunity Sample', 'url' => ['/site/opportunity']],
                            isset($_SESSION['publicId']) ? (
                            ['label' => 'Logout (' . \Yii::$app->session->get('publicId') . ') ', 'url' => ['/site/closesearch']]
//                    '<li>'
//
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . \Yii::$app->session->get('publicId'). ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
                            ) : (['label' => 'Login', 'url' => ['/site/login']])

//            Yii::$app->user->isGuest ? (
//                ['label' => 'Login', 'url' => ['/site/login']]
//            ) : (
//                '<li>'
//                . Html::beginForm(['/site/logout'], 'post')
//                . Html::submitButton(
//                    'Logout (' . Yii::$app->user->identity->username . ')',
//                    ['class' => 'btn btn-link logout']
//                )
//                . Html::endForm()
//                . '</li>'
//            )
                        ],
                    ]);
                    NavBar::end();
                    ?>
                </div>
            </div>

        </div>
    </nav>
