<?php

namespace app\models;

use GuzzleHttp\Client;
//use yii\db\ActiveRecord;
//use yii\web\IdentityInterface;

//use yii\web\IdentityInterface;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
//class User extends ActiveRecord implements IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];


    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Please choose a username.'],
        ];
    }

    public static function confirmUsername($username)
    {

        $client = new Client();

        $url = \Yii::$app->params['bioEndpoint'];

        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        return $body;

    }


    public function getUsername(){
        return $this->username;
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {

//        return true;
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
//        return $this->username;
//        return  self::confirmUsername($id);
//        $user = self::confirmUsername($id);
        // if (!count($user)) {
        //     return null;
        // }
//        return new static($user);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return  self::confirmUsername($username);

//        return static::findOne([
//            'phone' => $phone
//        ]);
//        $this->confirmUsername($username);
////        foreach (self::$users as $user) {
////            if (strcasecmp($user['username'], $username) === 0) {
////                return new static($user);
////            }
////        }
//
//        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->username;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
