<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 6/24/2020
 * Time: 4:53 PM
 */

namespace app\models;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class TorreApiEndpont
{
    // End Point using Get
	public function endpointRequest($url)
	{
	    $client = new Client();

	    try {
			$response = $client->request('GET', $url);

		} catch (\Exception $e) {
		    //Get all the the error
            return [];
		}
		return $this->response_handler($response->getBody()->getContents());
	}

	//Handling all the Response
	public function response_handler($response)
	{
		if ($response) {
			return json_decode($response,true);
		}

		return [];
	}

    //Bio Endpoint
    public function bio($username)
    {
        //Getting the Bio Endpoint
        $url = \Yii::$app->params['bioEndpoint'].$username;

        return $this->endpointRequest($url);
    }

    //Opportunity Endpoint
    public function opportunity($id)
    {
        //Getting Opportunity Endpoint
        $url = \Yii::$app->params['opportunityEndpoint'] . $id;

        return $this->endpointRequest($url);

    }

}
