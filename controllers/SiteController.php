<?php

namespace app\controllers;

use app\models\TorreApiEndpont;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
//        return $this->render('index');
        if (isset($_SESSION['publicId'])) {

            return $this->render('index');

        } else {

            return $this->redirect(['login']);

        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'loginlayout';

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {

            $endpoint = new TorreApiEndpont();

            $feedback = $endpoint->bio($model->username);

            if (!empty($feedback)) {
                \Yii::$app->session->set('publicId', $model->username);

                return $this->redirect('about');

            } else {

                return $this->render('login', [
                    'model' => $model,
                ]);
            }

        }

//        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionClosesearch()
    {
        unset($_SESSION['publicId']);
        return $this->redirect(['login']);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionOpportunity()
    {
        $endpoint = new TorreApiEndpont();

        $id = "0wxYeNd2";

        $opportunity = $endpoint->opportunity($id);

        if (!empty($opportunity)) {

            return $this->render('contact', [
                'opportunity' => $opportunity,
            ]);

        } else {

            Yii::$app->session->setFlash('Opportunity Not Available');

            return $this->redirect(['about']);
        }

    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        if (isset($_SESSION['publicId'])) {

            $endpoint = new TorreApiEndpont();

            $bio = $endpoint->bio($_SESSION['publicId']);

            return $this->render('about', [
                'bio' => $bio,
            ]);

        } else {

            return $this->redirect(['login']);

        }

    }
}
