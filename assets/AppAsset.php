<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
        //VENDOR CSS
        'theme/assets/vendor/bootstrap/css/bootstrap.min.css',
        'theme/assets/vendor/font-awesome/css/font-awesome.min.css',
        'http://www.wrraptheme.com/templates/lucid/html/assets/vendor/animate-css/animate.min.css',

        //MAIN CSS
        'theme/h-menu/assets/css/main.css',
        'theme/h-menu/assets/css/color_skins.css',

    ];
    public $js = [
        //Javascript
        'theme/h-menu/assets/bundles/libscripts.bundle.js',
        'theme/h-menu/assets/bundles/vendorscripts.bundle.js',


        'theme/assets/vendor/nestable/jquery.nestable.js', // <!-- Jquery Nestable -->

        'theme/h-menu/assets/bundles/mainscripts.bundle.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
