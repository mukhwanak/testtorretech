<?php
/**
 * User: Kennedy Mukhwana Maikuma Lingo
 * Date: 6/24/2020
 * Time: 5:00 PM
 */

namespace app\assets;

use yii\web\AssetBundle;

class LoginAsset extends AssetBundle
{
     public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [

        /*Begin VENDOR CSS*/
        'theme/assets/vendor/bootstrap/css/bootstrap.min.css',
        'theme/assets/vendor/font-awesome/css/font-awesome.min.css',
        /*End VENDOR CSS*/

        /* BEGIN MAIN CSS */
        'theme/h-menu/assets/css/main.css',
        'theme/h-menu/assets/css/color_skins.css',
        /* END MAIN CSS */

    ];
    public $js = [
        'theme/h-menu/assets/bundles/libscripts.bundle.js',
        'theme/h-menu/assets/bundles/vendorscripts.bundle.js',
        'theme/h-menu/assets/bundles/mainscripts.bundle.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

}
