<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    /*Torre Api End Points*/
    'bioEndpoint'=>'https://torre.bio/api/bios/',
    'opportunityEndpoint'=>'https://torre.co/api/opportunities/'
];
